const fs = require("fs")
const path = require("path")

const findPath = (fileName) => path.join(__dirname, fileName)

function readFile (path, cb)  {
    if (path && cb) {
        fs.readFile(findPath(path), "utf8", cb)
    }
}


function convertToUpperCase (newFile, data, cb)  {
  const upper = data.toUpperCase();
  fs.writeFile(newFile, upper, () => {
    console.log("file created.");
    fs.appendFile(findPath("filenames.txt"), newFile + ",", () => {
      console.log("file added to filenames.txt");
      cb(upper);
    });
  });
};


function convertToLowerCase (newFile, data, cb)  {
  const newData = data.toLowerCase().replaceAll(". ", "\n");
  fs.writeFile(newFile, newData, () => {
    console.log( "file created.");
    fs.appendFile(findPath("filenames.txt"), newFile + ",", () => {
      console.log(" file added to filenames.txt");
      cb(newData);
    });
  });
};


function sortingData (newFile, data, cb)  {
  const sorted = data.split("\n").sort();
  fs.writeFile(newFile, sorted.toString(), () => {
    console.log(" file created.");
    fs.appendFile(findPath("filenames.txt"), newFile, () => {
      console.log("file added to filenames.txt");
      cb();
    });
  });
};


function deletionOfNewFiles (cb)  {
  readFile("filenames.txt", (err, data) => {
    if (err) {
      console.log(err);
    }
    const names = data.split(",");
    names.forEach((element) => {
      const elementPath = path.resolve(element);
      fs.unlink(elementPath, () => {
        cb(element);
      });
    });
  });
};

module.exports = {
  readFile,
  convertToUpperCase,
  convertToLowerCase,
  sortingData,
  deletionOfNewFiles
};