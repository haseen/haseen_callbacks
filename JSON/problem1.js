const fs = require("fs");
const path = require("path");

function createDirectory(dir) {
  let newPath = path.join(__dirname, dir);
  fs.mkdir(newPath, function (err) {
    if (err) {
      console.error("Couldn't create directory'");
    } else {
      console.log("Created directory");
    }
  });
}
// createDirectory()

function createJsonFiles(path, data) {
  fs.writeFile(path, data, "utf8", function (err) {
    if (err) {
      console.log("Error creating file");
    } else {
      console.log("Created file");
    }
  });
}
// createJsonFiles()

function deleteJsonFiles(allFiles) {
  let rd = allFiles.length;
  allFiles.forEach(function (file) {
    fs.unlink(file, function (err) {
      if (err) {
        console.log("Error deleting");
      } else console.log("Successfully deleted");
    });
  });
}

module.exports = { createDirectory, createJsonFiles, deleteJsonFiles };
